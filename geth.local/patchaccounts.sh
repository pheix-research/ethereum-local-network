#!/bin/bash

ME=`basename "$0"`
CLIENT=geth
DSFN=genesis.json
N1CONF=node1.toml
N2CONF=node2.toml
EXT=patched
WORKPATH=$1
NDRPCPORTBASE=$2
NODEPORTS=()
NODESPIDARRAY=()
GETHSTARTDELAY=20
GETHTIMEOUT=10
KILLSIG=2
CHAINID=
GASLIMIT=12000000
SEALPERIOD=5
BALANCE=5000000000000000000000
DEFAULTNAMESPACE='"personal", "eth", "net", "web3", "txpool", "miner", "debug", "clique"'

if [ -z "$WORKPATH" ]; then
    WORKPATH=`pwd`
else
    if [ ! -d "$WORKPATH" ]; then
        WORKPATH=`pwd`
    fi
fi

if [ -z "$NDRPCPORTBASE" ]; then
    NDRPCPORTBASE=8450
fi

if [ -z "$NAMESPACESELECTOR" ]; then
    NAMESPACESELECTOR=test
fi

echo "***INF[$ME]: working within <${WORKPATH}> directory"

PATCHPATH=$WORKPATH/patched
REINIT=$WORKPATH/reinit.sh
DEMOSPEC=`cat $WORKPATH/$DSFN`

if [ ! -z "$PUBLICTESTNET" ] && [ $PUBLICTESTNET == "holesky" -o $PUBLICTESTNET == "sepolia" ]; then
    if [ $PUBLICTESTNET == "holesky" ]; then
        echo "***INF[$ME]: set chainid for holesky"
        CHAINID=17000
    elif [ $PUBLICTESTNET == "sepolia" ]; then
        echo "***INF[$ME]: set chainid for sepolia"
        CHAINID=11155111
    fi
else
    if [ ! -z "$EXTERNALCHAINID" ] && [ ${EXTERNALCHAINID} -gt 11982 ]; then
        CHAINID=${EXTERNALCHAINID}
    else
        CHAINID=1982
    fi
fi

if [ ! -z "$GETHSTARTDELAYUPDATE" ] && [ ${GETHSTARTDELAYUPDATE} -gt 0 ]; then
    GETHSTARTDELAY=${GETHSTARTDELAYUPDATE}
fi

if [ ! -z "$SEALPERIODUPDATE" ] && [ ${SEALPERIODUPDATE} -gt 0 ]; then
    SEALPERIOD=${SEALPERIODUPDATE}
fi

if [ ! -z "$GASLIMITUPDATE" ] && [ ${GASLIMITUPDATE} -gt 1000000 ]; then
    GASLIMIT=${GASLIMITUPDATE}
fi

if [ ! -z "$NDRPCPORTBASEUPDATE" ] && [ ${NDRPCPORTBASEUPDATE} -gt 2048 ] && [ ${NDRPCPORTBASEUPDATE} -lt 10000 ]; then
    NDRPCPORTBASE=${NDRPCPORTBASEUPDATE}
fi


if hash $CLIENT 2> /dev/null; then
    if [ -f $REINIT ]; then
        echo "***INF[$ME]: running $REINIT"
        $REINIT $WORKPATH
    fi

    for node in 1 2
    do
        NODEMODE=test
        NODENAMESPACE=$DEFAULTNAMESPACE
        NODECONFNAME=node${node}.toml
        NODEPORT=$((${NDRPCPORTBASE} + ${node} - 1))

        cat $WORKPATH/toml/$NODECONFNAME | sed "s|%nodeport%|$NODEPORT|g" | sed "s|%workpath%|$WORKPATH|g" | sed "s|%netid%|$CHAINID|g" | sed "s|%gas%|$GASLIMIT|g" | sed "s|%nodenamespace%|$NODENAMESPACE|g" > $WORKPATH/$NODECONFNAME

        $CLIENT --rpc.enabledeprecatedpersonal --config $WORKPATH/$NODECONFNAME > /dev/null 2>&1 & NODESPIDARRAY+=($!)

        NODEPORTS+=($NODEPORT)
    done

    echo "***INF[$ME]: child processes ${NODESPIDARRAY[0]} and ${NODESPIDARRAY[1]} are running geth nodes"
    echo "***INF[$ME]: network chainId=$CHAINID, gasLimit=$GASLIMIT, sealPeriod=$SEALPERIOD, portBase=$NDRPCPORTBASE, startDelay=$GETHSTARTDELAY"
else
    echo "***ERR[$ME]: could not run nodes - $CLIENT is not installed"
fi

sleep $GETHSTARTDELAY;

if [ ${NODESPIDARRAY[0]} -gt 0 ] && [ ${NODESPIDARRAY[1]} -gt 0 ]; then
    AUTH0=`curl -s --data '{"jsonrpc":"2.0","method":"personal_newAccount","params":["node0"],"id":0}' -H "Content-Type: application/json" -X POST localhost:${NODEPORTS[0]} | perl -lne 'print $1 if /"0x([0-9a-f]+)"/i'`
    AUTH1=`curl -s --data '{"jsonrpc":"2.0","method":"personal_newAccount","params":["node0"],"id":0}' -H "Content-Type: application/json" -X POST localhost:${NODEPORTS[0]} | perl -lne 'print $1 if /"0x([0-9a-f]+)"/i'`
    AUTH2=`curl -s --data '{"jsonrpc":"2.0","method":"personal_newAccount","params":["node1"],"id":0}' -H "Content-Type: application/json" -X POST localhost:${NODEPORTS[1]} | perl -lne 'print $1 if /"0x([0-9a-f]+)"/i'`
    if [ ! -z "$AUTH0" ] && [ ! -z "$AUTH1" ] && [ ! -z "AUTH2" ]; then
        echo "***INF[$ME]: node#0 (port ${NODEPORTS[0]}) account#1: 0x$AUTH0"
        echo "***INF[$ME]: node#0 (port ${NODEPORTS[0]}) account#2: 0x$AUTH1"
        echo "***INF[$ME]: node#1 (port ${NODEPORTS[1]}) account#1: 0x$AUTH2"

        LIST="$AUTH0,$AUTH2"

        if [ -f $PATCHPATH/$DSFN.$EXT ]; then
            rm -f $PATCHPATH/$DSFN.$EXT
        fi

        if [ ! -d $PATCHPATH ]; then
            mkdir $PATCHPATH
        fi

        SUBST1=`echo "${DEMOSPEC/\%netid\%/$CHAINID}"`
    	SUBST2=`echo "${SUBST1/\%sealperiod\%/$SEALPERIOD}"`
        SUBST3=`echo "${SUBST2/\%inital_signer_addr\%/$AUTH1$AUTH2}"`
    	echo "${SUBST3/\%accdata\%/\"$AUTH0\":\{\"balance\":\"$BALANCE\"\},\"$AUTH1\":\{\"balance\":\"$BALANCE\"\},\"$AUTH2\":\{\"balance\":\"$BALANCE\"\}}" >> $PATCHPATH/$DSFN.$EXT

        if [ -f $PATCHPATH/$DSFN.$EXT ]; then
            kill -$KILLSIG ${NODESPIDARRAY[0]}
            kill -$KILLSIG ${NODESPIDARRAY[1]}
            sleep $GETHTIMEOUT
            PS=`ps --no-headers -fp ${NODESPIDARRAY[0]},${NODESPIDARRAY[1]}`
            if [ -z "$PS" ]; then
                for node in 1 2
                do
                    NODEMODE=test
                    NODENAMESPACE=$DEFAULTNAMESPACE
                    NODEINDEX=$((node-1))
                    NODECONFNAME=node${node}.toml
                    NODEPORT=${NODEPORTS[NODEINDEX]}

                    if [ ! -z "$NAMESPACESELECTOR" ]; then
                        if [ "$NAMESPACESELECTOR" = "production" ] && [ "${node}" = "1" ]; then
                            NODENAMESPACE='"eth", "web3", "clique"'
                            NODEMODE=production
                        fi
                    fi

                    echo "***INF[$ME]: node ${node} is configured to <${NODEMODE}> mode on port ${NODEPORT} via ${WORKPATH}/${EXT}/${NODECONFNAME}"

                    rm -f $WORKPATH/$NODECONFNAME

                    cat $WORKPATH/toml/$NODECONFNAME | sed "s|%nodeport%|$NODEPORT|g" | sed "s|%workpath%|$WORKPATH|g" | sed "s|%netid%|$CHAINID|g" | sed "s|%gas%|$GASLIMIT|g" | sed "s|%nodenamespace%|$NODENAMESPACE|g" > $WORKPATH/$EXT/$NODECONFNAME

                    # NODECONF=$WORKPATH/node${node}.toml
                    # mv $NODECONF $WORKPATH/$EXT

    		        # init nodes
                    if [ -d $WORKPATH/geth-node${node}/geth ]; then
                        echo "***INF[$ME]: drop blockchain database for node #${node}"
                        rm -rf $WORKPATH/geth-node${node}/geth
                    fi

                    $CLIENT --datadir $WORKPATH/geth-node${node} init $PATCHPATH/$DSFN.$EXT > /dev/null 2>&1

                    echo "node${NODEINDEX}" > $PATCHPATH/node${node}.pwd
                done
                echo "***INF[$ME]: accounts in genesis are successfully patched and saved to $PATCHPATH/$DSFN.$EXT"
            else
                echo "***ERR[$ME]: accounts are not patched - geth processes are still alive, check <ps ax>"
            fi
        fi
    else
        echo "***ERR[$ME]: no response from geth nodes"
    fi
else
    echo "***ERR[$ME]: child processes not started, ids=${NODESPIDARRAY[0]},${NODESPIDARRAY[1]}"
fi
