#!/bin/sh

ME=`basename "$0"`
WORKPATH=$1
BACKUPFILE=keystore-backup-`date '+%Y-%m-%d_%H-%M-%S'`.tgz
BACKUPPATH=

if [ -z "$WORKPATH" ]; then
    WORKPATH=`pwd`
else
    if [ ! -d "$WORKPATH" ]; then
        WORKPATH=`pwd`
    fi
fi

echo "***INF[$ME]: working within <$WORKPATH> directory"

for NODE in 1 2
do
    NODEKEYPATH=${WORKPATH}/geth-node${NODE}/keystore

    if [ -d "${NODEKEYPATH}" ]; then
        BACKUPPATH=`echo "${NODEKEYPATH} ${BACKUPPATH}" | xargs`
    fi
done

if [ ! -z "${BACKUPPATH}" ]; then
    if [ ! -d "$WORKPATH/backup" ]; then
        mkdir "$WORKPATH/backup"
    fi

    tar -czf "${WORKPATH}/backup/${BACKUPFILE}" --absolute-names ${BACKUPPATH}

    if [ $? -eq 0 ]; then
        echo "***INF[$ME]: <${BACKUPPATH}> is backed up"
    fi
fi

for NODE in 1 2
do
    rm -rf ${WORKPATH}/geth-node${NODE}/keystore/*
    rm -rf ${WORKPATH}/geth-node${NODE}/geth
done

rm -rf $WORKPATH/patched
rm -rf $WORKPATH/genesis.json.patched
rm -rf $WORKPATH/*.toml
