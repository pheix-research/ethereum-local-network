#!/bin/bash

ME=`basename "$0"`
NETINSTPATH="$1"
NODE1IPC=geth-node1/geth.ipc
NODE2IPC=geth-node2/geth.ipc
ENODEREQ=

while getopts "p:r:" opt
do
    case $opt in
    p)
        NETINSTPATH=${OPTARG}

        if [ -d "${NETINSTPATH}" ]; then
            if [ ! -S "${NETINSTPATH}/${NODE1IPC}" ] || [ ! -S "${NETINSTPATH}/${NODE2IPC}" ]; then
                echo "***ERR[$ME]: IPC sockets ${NETINSTPATH}/${NODE1IPC} or ${NETINSTPATH}/${NODE2IPC} are not available"
                exit 1
            else
                echo "***INF[$ME]: IPC sockets are available"
            fi
        else
            echo "***ERR[$ME]: wrong local PoA network installation path ${NETINSTPATH}"
            exit 2
        fi
    ;;
    r)
        if [ ! -z ${OPTARG} ] ; then
            ENODEREQ=${OPTARG}
        fi
    ;;
    *)
        # echo "No reasonable options found!"
    ;;
    esac
done

if [ -z "${NETINSTPATH}" ]; then
    echo "***ERR[$ME]: blank local PoA network installation path"
    exit 3
fi

# https://stackoverflow.com/questions/54255980/how-to-close-netcat-connection-after-receive-a-server-response
if [ -z "${ENODEREQ}" ]; then
    ENODEREQ=`echo '{"jsonrpc":"2.0","method":"admin_nodeInfo","params":[],"id":0}' | nc -q 0 -U ${NETINSTPATH}/${NODE1IPC} | jq -r .result.enode`
fi

echo $ENODEREQ

RES=`echo '{"jsonrpc":"2.0","method":"admin_addPeer","params":["'${ENODEREQ}'"],"id":0}' | nc -q 0 -U ${NETINSTPATH}/${NODE2IPC} | jq -r .result`

if [ "$RES" = "true" ]; then
    echo "***INF[$ME]: peers are binded"
else
    echo "***ERR[$ME]: binding is failed"
    echo $RES

    exit 4;
fi

exit 0;
