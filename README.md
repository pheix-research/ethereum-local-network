# Sample Ethereum local network

## Overview

This repo contains setup suite for sample ethereum local network.

## License information

This is free and open source software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Links and credits

[Pheix CMS with data storing on Ethereum blockchain](https://gitlab.com/pheix-pool/core-perl6)

[Raku Net::Ethereum](https://gitlab.com/pheix/net-ethereum-perl6)

[Smart contracts repo](https://gitlab.com/pheix-research/smart-contracts)

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
