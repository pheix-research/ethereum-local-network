#!/bin/bash

ME=`basename "$0"`
NETINSTPATH=/ethereum-local-network/geth.local
ND0=$NETINSTPATH/start.node1
ND1=$NETINSTPATH/start.node2
REINIT=$NETINSTPATH/reinit.sh
PATCH=$NETINSTPATH/patchaccounts.sh
BIND=$NETINSTPATH/bindpeers.sh
NODE0PID=0
NODE1PID=0
GETHTIMEOUT=10
RUNNODE=
NODE1PORT=8540
NODE2PORT=8541
NAMESPACESELECTOR=test

while getopts "mnp:s:" opt
do
    case $opt in
    m)
        # node namespace specification
        NAMESPACESELECTOR=production
    ;;
    n)
        # patch accounts
        DOPATCH=nopatch
    ;;
    p)
        REGEXPR='^[0-9]+$'

        if [[ ${OPTARG} =~ $REGEXPR ]] ; then
            if [ ${OPTARG} -gt 2048 ] && [ ${OPTARG} -lt 10000 ] ; then
                NODE1PORT=${OPTARG}
                NODE2PORT=$((${OPTARG} + 1))

                echo "***INF[$ME]: override local ports to ${NODE1PORT} and ${NODE2PORT}"
            else
                echo "***WRN[$ME]: override port base range 2049 .. 9999"
            fi
        fi
    ;;
    s)
        if [ ${OPTARG} -eq 1 ] || [ ${OPTARG} -eq 2 ] ; then
            RUNNODE=${OPTARG}
        fi
    ;;
    *)
        # echo "No reasonable options found!"
    ;;
    esac
done

if [ $? -ne 0 ]; then
    echo "***ERR[$ME]: command line args processing failure"
    exit 3
fi

PORT1=`bash -c "exec 6<>/dev/tcp/127.0.0.1/${NODE1PORT}" &>/dev/null || echo ${NODE1PORT}`
PORT2=`bash -c "exec 6<>/dev/tcp/127.0.0.1/${NODE2PORT}" &>/dev/null || echo ${NODE2PORT}`

if [ -z "$PORT1" ] && [ -z "$PORT2" ]; then
    echo "***INF[$ME]: local ethereum network is already up"
    exit 0
fi

if [ -f $ND0 ] && [ -f $ND1 ] && [ -f $REINIT ] && [ -f $PATCH ] && [ -f $BIND ]; then
    echo "***INF[$ME]: running $PATCH"
    if [ "$DOPATCH" == "nopatch" ]; then
        echo "***INF[$ME]: patching is skipped"
    else
        export NAMESPACESELECTOR=$NAMESPACESELECTOR
        $PATCH $NETINSTPATH $NODE1PORT
        unset NAMESPACESELECTOR
    fi

    if [ -z "$RUNNODE" ]; then
        $ND0 $NETINSTPATH > /dev/null 2>&1 & NODE0PID=$!
        $ND1 $NETINSTPATH > /dev/null 2>&1 & NODE1PID=$!

        echo "***INF[$ME]: child processes $NODE0PID and $NODE1PID are running geth nodes"
        sleep $GETHTIMEOUT
        echo "***INF[$ME]: running <$BIND -p $NETINSTPATH>"
        $BIND -p $NETINSTPATH
    else
        if [ $RUNNODE -eq 1 ]; then
            $ND0 $NETINSTPATH > /dev/null 2>&1 & NODE0PID=$!
            echo "***INF[$ME]: child process $NODE0PID is running in single mode"
        else
            $ND1 $NETINSTPATH > /dev/null 2>&1 & NODE1PID=$!
            echo "***INF[$ME]: child process $NODE1PID is running in single mode"
        fi

        sleep $GETHTIMEOUT
    fi
else
    echo "***ERR[$ME]: could not run nodes - check <ethereum-local-network> installation"
    exit 1
fi

if [ $NODE0PID -gt 0 ] && [ $NODE1PID -gt 0 ]; then
    PSPID0=`ps ax | grep "[n]ode1.toml" | perl -lne 'print $1 if /^\s*([0-9]+)\s/i'`
    PSPID1=`ps ax | grep "[n]ode2.toml" | perl -lne 'print $1 if /^\s*([0-9]+)\s/i'`
    echo "***INF[$ME]: to quit nodes run <kill -9 $NODE0PID $NODE1PID $PSPID0 $PSPID1>"
else
    if [ -z "$RUNNODE" ]; then
        echo "***ERR[$ME]: child processes not started, ids=$NODE0PID,$NODE1PID"
        exit 2
    else
        if [ $NODE0PID -gt 0 ]; then
            PSPID0=`ps ax | grep "[n]ode1.toml" | perl -lne 'print $1 if /^\s*([0-9]+)\s/i'`
            echo "***INF[$ME]: to quit node 1 run <kill -9 $NODE0PID $PSPID0>"
        fi
        if [ $NODE1PID -gt 0 ]; then
            PSPID1=`ps ax | grep "[n]ode2.toml" | perl -lne 'print $1 if /^\s*([0-9]+)\s/i'`
            echo "***INF[$ME]: to quit node 2 run <kill -9 $NODE1PID $PSPID1>"
        fi
    fi
fi
